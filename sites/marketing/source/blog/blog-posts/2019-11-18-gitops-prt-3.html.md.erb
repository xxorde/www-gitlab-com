---
title: "How to deploy to any cloud using GitLab for GitOps"
author: Sara Kassabian
author_gitlab: skassabian
author_twitter: sarakassabian
categories: engineering
image_title: '/images/blogimages/gitopsseries.png'
description: "Why multi-cloud compatibility supports a clean GitOps workflow."
tags: git, kubernetes, workflow
guest: true
ee_cta: false
install_cta: false
twitter_text: "Maintain a streamlined workflow by deploying your application to any cloud using GitLab"
featured: yes
postType: content marketing
cta_button_text: 'Watch: GitOps expert panel'
cta_button_link: '/why/gitops-infrastructure-automation/'
merch_banner_destination_url: "/resources/guide-to-the-cloud/"
merch_banner_image_source: "/images/merchandising-content/mc-guide-to-app-security-ebook-vertical.png"
merch_banner_body_title: "Guide to the cloud"
merch_banner_body_content: "Harness the power of the cloud with microservices, cloud-agnostic DevOps, and workflow portability."
merch_banner_cta_text: "Learn more"
merch_sidebar_destination_url: "/resources/guide-to-the-cloud/"
merch_sidebar_image_source: "/images/merchandising-content/mc-guide-to-app-security-ebook-horizontal.png"
merch_sidebar_body_title: "Guide to the cloud"
merch_sidebar_body_content: "Harness the power of the cloud with microservices, cloud-agnostic DevOps, and workflow portability."
merch_sidebar_cta_text: "Learn more"
related_posts:
  - "/blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/"
  - "/topics/gitops/gitops-gitlab-collaboration/"
  - "/blog/2020/04/17/why-gitops-should-be-workflow-of-choice/"
---

_While there are plenty of DevOps tools that can fulfill some of the [functions of GitOps](/solutions/gitops/), GitLab is the only tool that can take your application from idea to code to deployment all in one collaborative platform. GitLab strategic account leader Brad Downey shows users how we make GitOps work in a three-part series. Here, Brad demonstrates how GitLab’s [Auto DevOps](/stages-devops-lifecycle/auto-devops/) function deploys to any cloud using Kubernetes and GitLab CI. New to our series? Go back and read [how GitLab powers GitOps processes](/topics/gitops/gitops-gitlab-collaboration/), and [how to use GitLab for infrastructure](/topics/gitops/gitlab-enables-infrastructure-as-code/)._

[GitOps](/topics/gitops/) is a workflow that uses a Git repository as the single source of truth for all infrastructure and application deployments. There are benefits beyond version control when using GitLab as the core repository. GitLab is a powerful tool that empowers your team to practice good GitOps procedures through its collaborative platform, ease of infrastructure deployments, as well as its multicloud compatibility.

[Brad Downey](/company/team/#bdowney), strategic account leader at GitLab, shows how to deploy applications to three Kubernetes servers using one common workflow. We successfully deploy the applications using Auto DevOps, powered by GitLab CI, with Helm and Kubernetes.

 <!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube-nocookie.com/embed/heQ1WY_08Tc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Watch the demonstration to see how to replicate this application deployment process.
{: .note.text-center}

# GitOps demo overview

First, we open the [gitops-demo group README.md file](https://gitlab.com/gitops-demo/readme), which shows the structure of the gitops-demo group. There are a few projects and two subgroups: infrastructure and [applications](https://gitlab.com/gitops-demo/apps). The [previous blog post focused on infrastructure](/topics/gitops/gitlab-enables-infrastructure-as-code/), while this demonstration dives deeper in application deployments.

![GitOps demo map](/images/blogimages/gitops_series_2019/gitops_demo.png){: .shadow.center.medium}

This image is a map of the repository that is the basis for our GitOps demo.
{: .note.text-center}

## Inside the applications folder

Brad created four different applications for this demo: my-asp-net-app1; my-spring-app2; my-ruby-app3; my-python-app4. There are also three different Kubernetes clusters, which each correspond to a different cloud environment: Microsoft Azure (AKS), Amazon (EKS), and Google Cloud (GKE).

By clicking the Kubernetes button on the left-hand corner, we can see that all of the Kubernetes clusters are registered to GitLab. The environmental scopes represent which application is deployed to each cloud.

### ASP.NET application on AKS

#### AutoDevOps at work

[The first example](https://gitlab.com/gitops-demo/apps/my-asp-net-app1) is an ASP.NET application, which is the equivalent of a Hello, World app – in other words, it’s nothing too fancy. There are a few modifications that are specific to how we’re deploying this application, which lives in the [application CI file](https://gitlab.com/gitops-demo/apps/my-asp-net-app1/blob/master/.gitlab-ci.yml), which is where the magic of GitLab’s Auto DevOps feature lives.

“The first thing we do is import the main Auto DevOps template,” says Brad. “We set a couple of variables, override a few commands for stages that are more applicable to .net code, and finally at the bottom here, I see that I've set my environment automatically to deploy production into AKS.”

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml

variables:
  DEPENDENCY_SCANNING_DISABLED: "true"

test:
  stage: test
  image: microsoft/dotnet:latest
  script:
    - 'dotnet test --no-restore'

license_management:
  stage: test
  before_script:
    - sudo apt-get update
    - sudo apt-get install -y dotnet-runtime-2.2 dotnet-sdk-2.2

production:
  environment:
    name: aks/production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
```
{: .language-yaml}

The pipeline will run automatically after a commit and deploy successfully, but we can [take a peek inside the pipeline](https://gitlab.com/gitops-demo/apps/my-asp-net-app1/pipelines/88314435) to see how it works.

![GitOps demo map](/images/blogimages/gitops_series_2019/app1_pipeline.jpg){: .shadow.center.medium}

The stages of the pipeline from build to production for the ASP.NET application.
{: .note.text-center}

A quick look inside the pipeline shows that all the jobs passed successfully. The Auto DevOps feature kicked off the build stage, which creates a Docker container and uploads it to the built-in Docker registry. The test phase is comprehensive and includes [container scanning, license management, SAST](https://about.gitlab.com/stages-devops-lifecycle/secure/), and unit tests.

Click the security and license tabs to dive deeper into the testing results, if you wish. The application deploys to production in the final stage of the pipeline.

#### Inside the AKS cluster

The ASP.NET application is deploying to the AKS cluster. Go to Operations > Environments to see the environment configured for this application. Metrics such as the HTTP error rates, latency rates, and throughput are available because Prometheus is already integrated into GitLab's Kubernetes clusters.

The environment can be launched directly from here; just click the live URL to see the application running on AKS. There isn’t a lot of extra code beyond what is already configured into GitLab that tells the application how to deploy. The Auto DevOps feature creates a Helm chart and deploys it to Kubernetes and AKS.

### Java Spring application on GKE

Brad configured the [Spring application](https://gitlab.com/gitops-demo/apps/my-spring-app2) similarly as the ASP.NET application by using a Dockerfile. Brad added the [Dockerfile](https://gitlab.com/gitops-demo/apps/my-spring-app2/blob/master/Dockerfile) into the repository root directory.

```docker
ROM maven:3-jdk-8-alpine
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN mvn package
ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
```

The Spring application deployment differs from the ASP.NET application in one way: it does not need any overrides to the AutoDevOps template. It uses the default template, deploying to GKE instead of AKS. The workflow for application deployment is identical regardless of what cloud the application is being deployed to. This makes [Multi-Cloud](https://events19.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2019/co-located-events/#multicloudcon) easy.

“I was able to produce a very similar build, test, and production run in this environment,” says Brad. “I get the same metrics, the error rates, latencies, throughputs, etc., and in this case, my application is running automatically in a container on Kubernetes in my GKE cluster.”

### Python application on EKS

The final example is a [Python application](https://gitlab.com/gitops-demo/apps/my-python-app4) that deploys on EKS. The components are similar to the previous examples, and use [gitlab-ci.yml to change the production environment to EKS](https://gitlab.com/gitops-demo/apps/my-python-app4/blob/master/.gitlab-ci.yml), and a Dockerfile to prepare the Helm chart. There are also a few overrides.

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml
test:
  image: python:3.7
  script:
    - pip install -r requirements.txt
    - pip install pylint
    - pylint main.py
production:
  environment:
    name: eks/production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
```

The GitLab CI file tells the application to deploy on EKS.
{: .note.text-center}

```docker
FROM python:3.7
WORKDIR /app
ADD . /app/
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python", "/app/main.py"
```

The [Dockerfile](https://gitlab.com/gitops-demo/apps/my-python-app4/blob/master/Dockerfile) gets the Helm chart ready.
{: .note.text-center}

Just like in previous examples, the [pipeline](https://gitlab.com/gitops-demo/apps/my-python-app4/pipelines/88314654) runs the same way as in the other applications with build, test, and production phases. Once the application is deployed to EKS, you can open up the live link and see the Python application in your browser window.

In the end, we can see that GitLab is a true multi-cloud solution that enables businesses to make decisions about which cloud provider they want to use, without disparate workflows, while still maintaining good GitOps practices.

“All of this is a consistent interface with the exact same workflow, making it simple to deploy to any major cloud running Kubernetes integrated with GitLab.”

### GitLab for GitOps

Good GitOps procedure has you making a Git repository the single source of truth for all of the code. In our demonstration, there is no code that exists outside of the Git repository – with infrastructure code living in the infrastructure repo, and application code living in the application repo.

While any Git repository could suffice for good GitOps procedure, there are few DevOps tools that truly encompass the core pillars of GitOps: collaboration, transparency in process, and version control.

Tools like epics, issues, and merge requests which are the crux of GitLab foster communication and transparency between teams. Infrastructure teams can build code using Terraform or [Ansible templates](/blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/) in GitLab, and deploy to the cloud using GitLab CI. GitLab is the true multi-cloud solution, allowing teams to deploy an application to any cloud service using GitLab CI and Kubernetes without having to significantly augment their workflows.

_Thank you Brad Downey for recording the videos on which this series is based._

<%= partial "includes/blog/blog-merch-banner" %>
