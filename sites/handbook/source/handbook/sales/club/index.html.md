---
layout: handbook-page-toc
title: "GitLab President's Club"
description: "President's Club is an incentive trip to celebrate the success of our top Field Sales Professionals and others in the organization who helped support the team throughout the year"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

President’s Club is synonymous with excellence and rewards individuals for achieving specific goals. This event aligns with our values of Collaboration and Results which helps to attract high-performing Sales Reps and other roles that support the sales efforts and allow the company to grow at the expected rate. At GitLab, this reward is in the form of an incentive trip to celebrate the success of our top Field Sales Professionals and others in the organization who helped support the team throughout the year.

![tropical-destination](/handbook/sales/club/tropical2.jpg)

## How to Qualify

For GitLab's FY22 President's Club, 49 GitLab team members (along with 1 guest per team member) will be rewarded with an invitation to join the President's Club incentive trip in May 2022. Exact dates to follow. Team member start dates must be on or before 2021-08-01 to be eligible and still be employed by GitLab at the time of the incentive trip to receive the benefit. Selection criteria varies by role (see below).

## Eligibility Criteria

| Category | # of winners | Criteria | 
| ------ | ------ | ------ |
| Top Strategic Account Leaders | 12 | Ranking by % of annual full quota (full quota, non-ramped annual quota, prorated based on start date) as shown on the FY22 President's Club Dashboard (link coming soon). See ENT tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=210333776) for details. |
| Top Mid Market Account Executives | 6 | Ranking by % of annual full quota (full quota, non-ramped annual quota, prorated based on start date) as shown on the FY22 President's Club Dashboard (link coming soon). See MM tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=2130992) for details. |
| Top ENT, MM, SMB Area Sales Managers | 4 (2 ENT, 2 COM) | Top quota performance as shown on the FY22 President's Club Dashboard (link coming soon), ≥ 36 Rep months under management. See ENT & MM tabs of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=210333776) for details. |
| Top ENT, MM, SMB Director/AVP | 2 (1 ENT, 1 COM) | Top quota performance as shown on the FY22 President's Club Dashboard (link coming soon), ≥ 36 Rep months under management. See ENT & MM tabs of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=210333776) for details. |
| Top SMB Account Executive | 3 | Top Closed/Won Net ARR. See SMB tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=974153580) for details. |
| Top Solution Architects | 7 | VP/CRO selection. See CS tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=916403586) for details. |
| Top Inside Sales Rep | 1 | VP/CRO selection. See ISR tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=1292511725) for details. |
| Top Channel Sales Managers | 3 | Leader selection. See Channel/Alliances tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=577788595) for details. |
| Top Alliances | 1 | Leader selection. See Channel/Alliances tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=577788595) for details. |
| Top Technical Account Managers | 5 | Leader selection. See CS tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=916403586) for details. |
| Top Professional Services Engineers | 2 | Leader selection. See CS tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=916403586) for details. |
| Top Sales Development Reps | 2 | VP/CMO selection. See SDR tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=0) for details. |
| Top Sales Development Manager | 1 | VP/CMO selection. See SDR tab of [this doc](https://docs.google.com/spreadsheets/d/11lct1eoCU7iWCff7OB0xU0BIy1tQ6wLcU7aRsghnWlM/edit?ts=604be470#gid=0) for details. |
| Top CRO Organization Managers | 2 | VP/CRO selection. Managers in the CRO org that don’t fall under one of the other Club award categories. Sales leader nominations to be collected by 1/31/22. |
| MVPs (Non-Sales) | 2 | CRO staff selection. Sales leader nominations to be collected by 1/31/22. |

## Presidents Club FAQ

**What is the latest on President’s Club 2020?**

We continue to adapt to the global health and travel challenges, and after a lot of careful consideration, we made the difficult decision to postpone 2020 Presidents Club and combine 2020 and 2021 winners in the same event. 

**What are the dates and location of President’s Club 2021?**

[President’s Club 2021](/handbook/sales/club/) will be at the [Ritz-Carlton in Maui, Hawaii](https://www.ritzcarlton.com/en/hotels/kapalua-maui) from November 30 - December 3, 2021.  

**What happens if I am a President’s Club recipient in both 2020 and 2021?**

If you are a President’s Club winner in <ins>both 2020 and 2021</ins>, you will receive two extra days at the Ritz Carlton in Maui for you and your guest. 

**What are the dates and location of President’s Club 2022?**

[President’s Club 2022](/handbook/sales/club/) will be at the [Ritz-Carlton Grand Cayman Islands Resort](https://www.ritzcarlton.com/en/hotels/caribbean/grand-cayman) in May 2022. Exact dates and schedule to be announced.  

**Do I get extra time if I’m traveling from EMEA/APAC?**

While we understand that the trip is long, there will not be extra time allotted to team members traveling from EMEA or APAC. However, you’re always welcome to book with our rate up to 3 days pre- and/or post-event on your own.  

**How do I qualify for President’s Club?**

Please see the [How to Qualify](/handbook/sales/club/#how-to-qualify) section above for PC 2022. 

**How are President’s Club winners selected?**

Please see the [Selection Criteria](/handbook/sales/club/#eligibility-criteria) section above for PC 2022. 

**What is the schedule for President’s Club 2020/2021?**

- November 30 - Travel Day & Welcome Reception
- December 1 - Planned activities; Club Dinner
- December 2 - Planned activities; Farewell Reception
- December 3 - Travel Day; two-time winners (2020 and 2021) will stay 
- December 4 - Relaxation day for two-time winners 
- December 5 - Travel Day for two-time winners

**If I am selected as a President’s Club winner, can I bring a guest?**

Yes! Your significant other or guest is invited (and encouraged!) to attend Club with you. Please note that guests must be at least 21 years of age. Club is something truly special – a time for us to step back and celebrate your incredible hard work and accomplishments – and we want your guest to be a part of that! 

**Is my guest’s travel covered by GitLab?**

Yes, this trip will be all-inclusive for both you and your guest. Travel, hotel, meals, and activities will be covered by GitLab.

**Can I bring my kids to Club?**

Club is an adult-only event that is intended for team members to spend time together, celebrate, and build relationships on the foundation of their success and contribution to the company. 

If you have a family member or child that requires 24 hour care, or some other unique circumstance that jeopardizes your ability to attend, we can explore exceptions. For additional information, please reach out to Cheri Holmes and Libby Schulze via email. 

**When will we announce Club 2022 winners?**

Winners for President’s Club 2022 will be announced during the Awards Dinner at Sales Kickoff (SKO) taking place in February 2022. For more information about SKO, please see [this Handbook page](/handbook/sales/training/SKO/).

Reference: [GitLab Events Code of Conduct](https://about.gitlab.com/company/culture/contribute/coc/)

