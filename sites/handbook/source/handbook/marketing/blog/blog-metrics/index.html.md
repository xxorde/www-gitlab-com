---
layout: handbook-page-toc
title: "Blog dashboard"
description: "How to use the GitLab blog performance dashboard."
---

You can find the [GitLab Blog Dashboard here](https://datastudio.google.com/s/vENymsekyR0). Please watch the video below to get an overview of what metrics and KPIs are included and how to use the dashboard.
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/jJnUmKaxu5g" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
Most reports in the dashboard include notes to explain what you're seeing and how you can modify or go deeper into the data. Hovering over items usually presents more information, such as totals and percentage changes. 

You can learn more about our other [website analytics dashboards here](/handbook/marketing/inbound-marketing/search-marketing/analytics/).
