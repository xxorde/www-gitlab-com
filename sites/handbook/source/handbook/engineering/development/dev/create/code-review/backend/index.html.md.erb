---
layout: handbook-page-toc
title: "Create:Code Review BE Team"
description: The Create:Code Review BE team is responsible for all backend aspects of the product categories that fall under the Code Review group of the Create stage.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Create:Code Review BE team is responsible for all backend aspects of the product categories that fall under the [Code Review group][group] of the [Create stage][stage] of the [DevOps lifecycle][lifecycle].

[group]: /handbook/product/categories/#code-review-group
[stage]: /handbook/product/categories/#create-stage
[lifecycle]: /handbook/product/categories/#devops-stages

## Group Members

The following people are permanent members of the Create:Code Review Group:

<%= direct_team(manager_role: 'Backend Engineering Manager, Create:Code Review') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] (Create(?!:)|Create:Code Review)/, direct_manager_role: 'Backend Engineering Manager, Create:Code Review') %>

## Sisense & KPIs
<%#= TODO: Ensure dashboard exists at https://app.periscopedata.com/app/gitlab/561630/Dev-Sub-department-Overview-Dashboard #>
<%#= partial("handbook/engineering/development/dev/create-code-review-be/metrics.erb") %>
Coming Soon

## Hiring

This chart shows the progress we're making on hiring. Check out our
[jobs page](/jobs/) for current openings.

<%= hiring_chart(department: 'Create:Code Review BE Team') %>

### Team OKRs

Objectives and Key Results (OKRs) help align our team towards what really matters. These happen quarterly, align up through the stage, and are based on [company OKRs](/company/okrs/) and the [engineering OKR process](/handbook/engineering/#engineering-okr-process). We check in on the status of our progress routinely throughout the quarter - usually on a weekly basis, but at least on a monthly basis, to determine whether we are on track or need to pivot in order to accomplish or change these goals. At the end of the quarter, we do a [final scoring](/company/okrs/#scoring-okrs) which includes a [retrospective](/handbook/engineering/#okr-retrospection) on how the quarter went according to these OKRs. Individuals within the team, with the exception of Staff Engineers, are not required to have their own OKRs, but it is strongly encouraged in order to provide an overarching direction for the quarter and realize the impact we are making on the company as a whole. In some cases, it may also make sense to assign a key result directly to an individual in order to have a [DRI](/handbook/people-group/directly-responsible-individuals/).

Example template with questions to ask yourself: 

- **Objective:** How are you going to move X forward? _("Become a hero by improving efficiency")_
  - **KR:** If you have met your objective, what will the outcome be? _("Mean Time to Resolve is reduced from 5 days to 2 days")_
  - **KR:** How will I accomplish this objective? _("Automate monitoring alerts so we don't have to check dashboards")_
  - **KR:** Why will this KR matter? _("Team efficiency improves by 5%")_

[For a list of current and past OKRs for our team, use this link.](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acode%20review&label_name[]=OKR&label_name[]=backend)

## Training and Development Opportunities

This is a list of commonly requested training and some additional resources:

1. [The Database maintainer template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/trainee-database-maintainer.md) centralizes training related to being not just a maintainer, but all the resources for a reviewer as well.
1. [The Golang training template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/golang_training.md) offers resources related to learning Go.
1. [The feature flag training template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/feature-flag-training.md) will help get you started in understanding how to use feature flags at GitLab, as well as monitor the performance of your work.
1. [There are 2 YouTube playlists](/handbook/engineering/monitoring/#related-videos) that cover our monitoring and visualization tools. More resources are available [in the handbook](/handbook/engineering/#visualization-tools) in terms of Grafana, Kibana, Prometheus, Sentry, and Sitespeed.io.
1. [There is a DIB training template](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/diversity-inclusion-belonging-training-template.md) that utilizes internal resources and LinkedIn Learning, as well as an additional [DIB certification course](https://gitlab.edcast.com/journey/dib-training-certification) which is offered directly from GitLab.
1. [The "transitioning to a manager" page](/handbook/people-group/learning-and-development/manager-development/) and related training issue helps provide an idea of what management is like at GitLab.
1. [Rails performance materials](https://gitlab.com/gitlab-org/memory-team/team-tasks/-/issues/35) (internal only) are available from a workshop that was done at GitLab.

### Additional resources

- [Live learning sessions](/handbook/people-group/learning-and-development/#learning-sessions)
- [Learning and Development - Learning Initiatves](/handbook/people-group/learning-and-development/learning-initiatives/)
- [Compliance courses](/handbook/people-group/learning-and-development/compliance-courses/)
- [Tuition reimbursement and continued learning expenses](/handbook/total-rewards/benefits/general-and-entity-benefits/#tuition-reimbursement)

## Work

In general, we use the standard GitLab [engineering workflow]. To get in touch
with the Create:Code Review BE team, it's best to create an issue in the relevant project
(typically [GitLab]) and add the `~"group::code review"` and `~backend` labels, along with any other
appropriate labels. Then, feel free to ping the relevant Product Manager and/or
Engineering Manager as listed above.

For more urgent items, feel free to use [#g_create_code_review] on Slack.

[Take a look at the features we support per category here.](/handbook/product/categories/features/#createcode-review-group)

[engineering workflow]: /handbook/engineering/workflow/
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[#g_create_code_review]: https://gitlab.slack.com/archives/g_create_code-review

### Working within our team

[Team members](/handbook/engineering/development/dev/create-code-review-be/#team-members) meet monthly and are encouraged to join in order to to interact with peers, solve technical challenges, and discuss freely with each other. You can check for the next meeting on the [Code Review Group calendar](https://calendar.google.com/calendar/embed?src=c_bt3jpkj6afr7321vpg9ra1191s%40group.calendar.google.com&ctz=America%2FChicago).

#### Tips and Tricks

For tips, tricks, or quick shell scripts that aren't "ready" or sufficient enough to add to our developer documentation or handbook, we use [the Create stage wiki](https://gitlab.com/groups/gitlab-com/create-stage/-/wikis/home).

### Working with product

Weekly calls between the product manager and engineering managers (frontend and backend) are listed in the "Code Review Group" calendar. Everyone is welcome to join and these calls are used to discuss any roadblocks, concerns, status updates, deliverables, or other thoughts that impact the group. Every 2 weeks (in the middle of a release), a mid-milestone check-in occurs, to report on the current status of `~"Deliverable"`s. Monthly calls occurs under the same calendar where the entire group is encouraged to join, in order to highlight accomplishments/improvements, discuss future iterations, review retrospective concerns and action items, and any other general items that impact the group.

### Collaborating with other counter parts

You are encouraged to work as closely as needed with stable counterparts outside of the PM. We specifically include quality engineering and application security counterparts prior to a release kickoff and as-needed during code reviews or issue concerns.

Quality engineering is included in our workflow via the [Quad Planning Process](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6318).

Application Security will be involved in our workflow at the same time that [kickoff emails](#kickoff-emails) are sent to the team, so that they are able to review the upcoming milestone work, and notate any concerns or potential risks that we should be aware of.


### Capacity planning

<%= partial("handbook/engineering/development/dev/create/capacity_planning.erb") %>

#### Availability

Approximately 5-10 business days before the start of a new release, the EM will begin determining how "available" the team will be. Some of the things that will be taken into account when determining availability are:

* Upcoming training
* Upcoming time off / holidays
* Upcoming on-call slots
* Potential time spent on another teams deliverables

Availability is a percentage calculated by _(work days available / work days in release) * 100_.

All individual contributors start with a "weight budget" of 10, meaning they are capable (based on historical data) of completing a maximum number of issues worth 10 weight points total (IE: 2 issues which are weighted at 5 and 5, or 10 issues weighted at 1 each, etc.) Then, based on their availability percentage, weight budgets are reduced individually. For example, if you are 80% available, your weight budget becomes 8.

Product will prioritize issues based on the teams total weight budget. Our [planning rotation](#planning-rotation) will help assign weights to issues that product intends on prioritizing, to help gauge the amount of work prioritized versus the amount we can handle prior to a kickoff.

#### Kickoff emails

Once availability has been determined, weights have been assigned, and the PM/EM finalize a list of prioritized issues for the upcoming release, kickoff emails will be sent. The intent of this email is to notify you of the work we intend to assign for the upcoming release. This email will be sent before the release begins. The kickoff email will include:

* Your availability, weight budget, and how it was calculated
* A list of the issues you will most probably be assigned as an individual
* A reasoning behind why you have been assigned more than your weight budget, if applicable
* A list of the issues the team is working on that are deemed "note-worthy," in case you'd like to offer help on those issues as time allows

Emails get sent to each individual contributor on the team, as well as the Application Security counterpart, in order to give a heads-up about the upcoming issues in the milestone and what the assignments will be.

#### Planning

> If you haven't read the code, you haven't investigated deeply enough
>
> -- <cite>Nick Thomas</cite>

To assign weights to issues in a future milestone, on every 4th of the development month (or the next working day if it falls on a holiday or weekend), BE engineers look at the list of issues that are set for next milestone. These are assigned by the engineering manager. To weight issues, before the 15th of the month, they should:

1. See if there is already a discussed backend solution/plan or none yet.
1. If the discussed backend solution/plan isn't that clear, clarify it.
1. If there's no solution/plan yet, devise one. Doesn't need to be a detailed solution/plan. Feel free to ask other people to pick their brains.
1. If there's a need to collaborate with stable counterpart to devise a solution/plan, add a comment and tag relevant counterparts.
1. Give issue a weight if there's none yet or update if the existing weight isn't appropriate anymore. Leave a comment why a certain weight is given.
1. It's strongly encouraged to spend no more than 2 hours per issue. Give it your best guess and move on if you run out of time.
1. Label the issue as ~"workflow::ready for development".
1. Unassign yourself or keep it assigned if you want to work on the issue.

#### Follow-up issues

You will begin to collect follow-up issues when you've worked on something in a release but have tasks leftover, such as technical debt, feature flag rollouts or removals, or non-blocking work for the issue. For these, you can address them in at least 2 ways:
* Add an appropriate future milestone to the follow-up issue(s) with a weight and good description on the importance of working this issue
* Add the issue(s) to the relevant [planning issue](https://gitlab.com/gitlab-org/create-stage/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acode%20review&search=planning)
 
You should generally take on follow-up work that is part of our [definition of done](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done), preferably in the same milestone as the original work, or the one immediately following. If this represents a substantial amount of work, bring it to your manager's attention, as it may affect scheduling decisions.

If there are many follow-up issues, consider creating an epic.

#### Backend and Frontend issues

Many issues require work on both the backend and frontend, but the weight of that work may not be the same. Since an issue can only have a single weight set on it, we use scoped labels instead when this is the case: `~backend-weight::<number>` and `~frontend-weight::<number>`.

### What to work on

<%= partial("handbook/engineering/development/dev/create/what_to_work_on.erb", locals: { group: "Code Review", slack_channel: 'g_create_code-review' }) %>

[issue board]: https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=group::code%20review&label_name[]=backend
[assignment board]: https://gitlab.com/groups/gitlab-org/-/boards/2142016

### Workflow labels

<%= partial("handbook/engineering/development/dev/create/workflow_labels.erb", locals: { group_label: 'group::code review' }) %>

### Async standup

<%= partial("handbook/engineering/development/dev/create/async_standup.erb") %>

Our team is encouraged to post links to their deliverable issues or merge requests when they are mentioned in relation to the second question. This helps other team members to understand what others are working on, and in the case that they come across something similar in the future, they have a good reference point.

### Retrospectives

We have 1 regularly scheduled "Per Milestone" retrospective, and can have ad-hoc "Per Project" retrospectives.

#### Per Milestone

<%= partial("handbook/engineering/development/dev/create/retrospectives.erb", locals: { group: "Code Review", group_slug: 'code-review' }) %>

#### Per Project

If a particular issue, feature, or other sort of project turns into a particularly useful learning experience, we may hold a synchronous or asynchronous retrospective to learn from it. If you feel like something you're working on deserves a retrospective:
1. [Create an issue](https://gitlab.com/gl-retrospectives/create-stage/code-review/issues) explaining why you want to have a retrospective and indicate whether this should be synchronous or asynchronous
2. Include your EM and anyone else who should be involved (PM, counterparts, etc)
3. Coordinate a synchronous meeting if applicable

All feedback from the retrospective should ultimately end up in the issue for reference purposes. %>

### Deep Dives

<%= partial("handbook/engineering/development/dev/create/deep_dives.erb") %>

### Career development

<%= partial("handbook/engineering/development/dev/create/career_development.erb", locals: { group: "Code Review" }) %>

#### Feedback

It can be hard to understand how you're doing in your role, because feedback can come off as formal (annual reviews, 360 surveys, career development conversations, goal check-ins) or casual (in Slack channels, 1-1's, MR reviews, team meetings.) We receive various kinds of feedback regularly and through different formats, so the type of feedback you're receiving is not always clear. In order to be more intentional about the types of feedback given, here is a classification chart based on [three types of feedback](https://forimpact.org/three-types-feedback/#:~:text=%E2%80%9CFeedback%20comes%20in%20three%20forms,about%20relationship%20and%20human%20connection.):

| Label | Meaning | Example |
| **(appreciation)** | I want to thank you for doing this, and please do more of it in the future | "I did not expect that you would have created a working group, because you've done so, our whole team will benefit from the results."
| **(coaching)** | I'm trying to help you improve a behavior you are already exhibiting or change a behavior that you currently have | "The reports that you give me are very helpful, and in the future we can schedule them for the first of the month to be more consistent."
| **(evaluation)** | Tells you where you stand according to existing standards or expectations | "My expectation was that our decision would be transparent. Since it was not, our team has forgotten the decision, so we must be sure and meet that expectation next time." |


### Performance Monitoring

The Create:Code Review BE team is responsible for keeping some API endpoints and
controller actions performant (e.g. below our target speed index).

There are Grafana and Kibana dashboards available that the team can check to
ensure we are meeting these targets.

Here are the Kibana dashboards that give a quick overview on how they perform:

- [Create::Code Review Controller Actions](https://log.gprd.gitlab.net/app/kibana#/visualize/edit/3a9682e0-205a-11eb-81e5-155ba78758d4?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-7d%2Cto%3Anow))) (internal only)
- [Create::Code Review: Endpoints](https://log.gprd.gitlab.net/app/kibana#/visualize/edit/55f01d50-28a0-11eb-af41-ad80f197fa45?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-7d%2Cto%3Anow))) (internal only)

These tables are filtered by the endpoints and controller actions that the group
handles and sorted by P90 (slowest first) for the last 7 days by default.

The [Grafana dashboard](https://dashboards.gitlab.net/d/stage-groups-code_review/stage-groups-group-dashboard-create-code-review?orgId=1)
shows more specific details about each actions and endpoints.

To see the specific details for certain actions/endpoints, it can be filtered
by `action` and/or `controller`.

### Calendar Invites

There are calendar invites that act as a reminder for backend team members
to check these dashboards weekly on Monday:

- APAC: Check performance dashboards - set on Monday 12AM UTC
- AMER: Check performance dashboards - set on Monday 4PM UTC

All backend team members are invited to appropriate calendar invites. This
does not necessarily mean that we only need to take a look at them at those
specific times.

If a team member sees that an action or endpoint does not meet our target,
they should create a performance issue if there's no existing one. If there's
an existing one, team member can either update the issue, investigate or work
on the issue (if they have spare time).
